# [witchcraft](https://hex.pm/packages/witchcraft)

![Hex.pm](https://img.shields.io/hexpm/dw/witchcraft)
Monads and other dark magic (monoids, functors, traversables, &c)

* Do-notation documentation in [algae](https://hex.pm/packages/algae) (![Hex.pm](https://img.shields.io/hexpm/dw/algae)) or (unofficial) [Witchcraft Monadic Do Notation Primer](https://gist.github.com/expede/59b4e7e49fd394210ca5f31c7f00f382)

# Similar packages
* ![towel](https://img.shields.io/hexpm/dw/towel) towel
* ![ok](https://img.shields.io/hexpm/dw/ok) ok
* ![exceptional](https://img.shields.io/hexpm/dw/exceptional) exceptional
* ![witchcraft](https://img.shields.io/hexpm/dw/witchcraft) witchcraft
* ![monadex](https://img.shields.io/hexpm/dw/monadex) monadex

# Unofficial documentation
* [*Functional Programming in Elixir with Witchcraft*
  ](https://blog.appsignal.com/2022/02/08/functional-programming-in-elixir-with-witchcraft.html)
  2022-02 Gints Dreimanis
* [*Witchcraft to get something similar to the IO Monad (in Haskell, Scala, etc) but in Elixir?*
  ](https://elixirforum.com/t/witchcraft-to-get-something-similar-to-the-io-monad-in-haskell-scala-etc-but-in-elixir/48710)
  (2022) (Elixir Forum)
